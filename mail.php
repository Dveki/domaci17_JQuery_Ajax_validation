<?php

if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $conf_email = $_POST['conf_email'];

    $errorEmpty = false;
    $errorEmail = false;

    if (empty($email) || empty($conf_email)) {
        echo "<span class='form-error'>Fill in email!</span>";
        $errorEmpty = true;
    }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) || !filter_var($conf_email, FILTER_VALIDATE_EMAIL)) {
        echo "<span class='form-error'>Mail is not valid form</span>";
        $errorEmail = true;
    }
    elseif ($email !==$conf_email) {
        echo "<span class='form-error'>Your email doesn't match</span>";
    }
    else {
        echo "<span class='form-success'>Thank you!</span>";
    }
}
else {
    echo "There was an error!";
}
?>

