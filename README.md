Kreirati web obrazac za pisanje komentara. Obrazac treba da
sadrži višelinijsko polje za unos teksta i polje za unos email
adrese. Unos komentara ograničiti na 250 karaktera.
Korisniku ispisivati u svakom trenutku koliko ima karaktera još
za unos. Nakon 250-tog karaktera ne treba dozvoliti dalji
unos. Validaciju ispravnosti email adrese uraditi preko JQuery
AJAX poziva