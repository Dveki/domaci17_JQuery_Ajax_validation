$(document).ready(function() {
var $$textField= $('#text_field');
$$textField.on ("input", function() {
  var $$length = $$textField.val().length
  var $maxlength=250;
  if ($$length <= $maxlength) {
        $('#chars').val($maxlength-$$length);
  }
  else 
  {$$textField.attr("maxlength", $maxlength)}
});
$("#formsubmit").validate ({
    rules: {
        email: {
            required: true,
            email:true
        },
        conf_email: {
            required: true,
            email: true,
            equalTo: '#email'
        }
    },
    messages: {
        email: {
            required: "We need your email address to contact you!",
            email: "Your email address must be in the format of name@domain.com"
        },
        conf_email:{
            required: "Please Repeat Your Email",
                email: "Your email address must be in the format of name@domain.com",
                equalTo: "Your email doesn't match"
        }
    }
});

$("form").submit(function(event){
    event.preventDefault();
    var email = $("#email").val();
    var conf_email = $("#conf_email").val();
    var submit = $("#mail-submit").val();
    var text_field =$("#text_field").val();
    $(".form-message").load("mail.php", {
        email: email,
        conf_email : conf_email,
        text_field: text_field,
        submit: submit
    });
});



});